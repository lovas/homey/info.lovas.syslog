import sourceMapSupport from 'source-map-support'
import syslog from 'syslog-client'
import Homey from 'homey'

sourceMapSupport.install()

class SyslogApp extends Homey.App {
  private syslogClient: syslog.Client | undefined
  private syslogServer: string
  private syslogPort: number
  private syslogTransport: string
  private syslogClientName: string
  private syslogClientOptions: { port: number; transport: syslog.Transport; syslogHostname: string; }
  private syslogTag: string
  private syslogTransportMap: Map<string, syslog.Transport> = new Map()

  constructor (...args: any) {
    super(...args)
    // add map values
    this.syslogTransportMap.set('udp', syslog.Transport.Udp)
    this.syslogTransportMap.set('tcp', syslog.Transport.Tcp)
    // default values
    this.syslogServer = '127.0.0.1'
    this.syslogPort = 514
    this.syslogTransport = 'udp'
    this.syslogClientName = '127.0.0.1'
    this.syslogClientOptions = {
      port: this.syslogPort,
      transport: this.syslogTransportMap.get(this.syslogTransport) || syslog.Transport.Udp,
      syslogHostname: this.syslogClientName
    }
    this.syslogTag = 'homey'
  }

  /**
   * onInit is called when the app is initialized.
   */
  async onInit (): Promise<void> {
    this.log('SyslogApp has been initialized')

    this.homey.settings.on('set', (args) => {
      if (args) {
        this.log(`onInit/${args}`)
        // todo: update syslog.Client to reflect settings update
        if (args === 'server') {
          this.syslogServer = this.homey.settings.get('server')
          this.log(`server updated: ${this.syslogServer}`)
        }
        if (args === 'port') {
          this.syslogPort = this.homey.settings.get('port')
          this.log(`port updated: ${this.syslogPort}`)
        }
        if (args === 'transport') {
          this.syslogTransport = this.homey.settings.get('transport')
          this.log(`transport updated: ${this.syslogTransport}`)
        }
        if (args === 'client') {
          this.syslogClientName = this.homey.settings.get('client')
          this.log(`client updated: ${this.syslogClientName}`)
        }
        if (args === 'tag') {
          this.syslogTag = this.homey.settings.get('tag')
          this.log(`tag updated: ${this.syslogTag}`)
        }
      }
    })

    if (this.homey.settings.get('server') !== undefined) {
      this.syslogServer = this.homey.settings.get('server')
      this.log(`logging to server: ${this.syslogServer}`)
    } else {
      this.log(`logging to default server: ${this.syslogServer}`)
    }

    if (this.homey.settings.get('port') !== undefined) {
      this.syslogPort = this.homey.settings.get('port')
      this.log(`logging to port: ${this.syslogPort}`)
    } else {
      this.log(`logging to default port: ${this.syslogPort}`)
    }

    if (this.homey.settings.get('transport') !== undefined) {
      this.syslogTransport = this.homey.settings.get('transport')
      this.log(`logging with transport: ${this.syslogTransport}`)
    } else {
      this.log(`logging to default transport: ${this.syslogTransport}`)
    }

    if (this.homey.settings.get('client') !== undefined) {
      this.syslogClientName = this.homey.settings.get('client')
      this.log(`logging from client: ${this.syslogClientName}`)
    } else {
      this.log(`logging from default client: ${this.syslogClientName}`)
    }
    if (this.homey.settings.get('tag') !== undefined) {
      this.syslogTag = this.homey.settings.get('tag')
      this.log(`logging with tag: ${this.syslogTag}`)
    } else {
      this.log(`logging with default tag: ${this.syslogTag}`)
    }

    this.syslogClientOptions = {
      syslogHostname: this.syslogClientName,
      transport: this.syslogTransportMap.get(this.syslogTransport) || syslog.Transport.Udp,
      port: this.syslogPort
    }
    this.syslogClient = syslog.createClient(this.syslogServer, this.syslogClientOptions)

    const msgInfo = this.homey.flow.getActionCard('log-info-message')
    msgInfo.registerRunListener(async (args, state) => {
      const message: string = args.Message || ''
      this.logInformation(message)
    })
    const msgWarn = this.homey.flow.getActionCard('log-warn-message')
    msgWarn.registerRunListener(async (args, state) => {
      const message: string = args.Message || ''
      this.logWarning(message)
    })
    const msgError = this.homey.flow.getActionCard('log-error-message')
    msgError.registerRunListener(async (args, state) => {
      const message: string = args.Message || ''
      this.logError(message)
    })

    const msgCustom = this.homey.flow.getActionCard('log-custom-message')
    msgCustom.registerRunListener(async (args, state) => {
      const severity: string = args.Severity || 'info'
      const message: string = args.Message || 'empty message'
      const tag: string = args.Tag || this.syslogTag

      this.logCustom(severity, tag, message)
    })
  }

  private logInformation (message: string) {
    this.logMessage(syslog.Severity.Informational, message)
  }

  private logWarning (message: string) {
    this.logMessage(syslog.Severity.Warning, message)
  }

  private logError (message: string) {
    this.logMessage(syslog.Severity.Error, message)
  }

  private logMessage (severity: syslog.Severity, message: string) {
    this.log(`severity: ${severity}, message: ${message}`)
    const msgOptions = {
      severity
    }
    const that = this
    if (this.syslogClient !== undefined) {
      this.syslogClient.log(`${this.syslogTag} ${message}`, msgOptions, (error) => {
        if (error) {
          that.error(error)
        } else {
          that.log('sent message successfully')
        }
      })
    } else {
      this.error('syslogClient is undefined')
    }
  }

  private logCustom (severityString: string, tag: string, message: string) {
    let severity: syslog.Severity
    switch (severityString) {
      case 'debug':
        severity = syslog.Severity.Debug
        break
      case 'info':
        severity = syslog.Severity.Informational
        break
      case 'notice':
        severity = syslog.Severity.Notice
        break
      case 'warning':
        severity = syslog.Severity.Warning
        break
      case 'error':
        severity = syslog.Severity.Error
        break
      case 'crit':
        severity = syslog.Severity.Critical
        break
      case 'alert':
        severity = syslog.Severity.Alert
        break
      case 'emerg':
        severity = syslog.Severity.Emergency
        break
      default:
        severity = syslog.Severity.Informational
    }

    const maxTagLength = 32
    tag = tag.replace(/[^0-9a-zA-Z]/g, '')
    tag = tag.substring(0, maxTagLength)

    this.log(`severity: ${severity}, tag: ${tag}, message: ${message}`)
    const msgOptions = {
      severity
    }
    const that = this
    if (this.syslogClient !== undefined) {
      this.syslogClient.log(`${tag} ${message}`, msgOptions, (error) => {
        if (error) {
          that.error(error)
        } else {
          that.log('sent message successfully')
        }
      })
    } else {
      this.error('syslogClient is undefined')
    }
  }
}

module.exports = SyslogApp
